﻿// ConsoleApplication3.cpp : 此檔案包含 'main' 函式。程式會於該處開始執行及結束執行。
//

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <gtest/gtest.h>

using namespace std;

#define replace01 15
#define replace02 0

int partition(vector<int>& src, int low, int high) {

    int bIndex = low - 1;
    for (int i = low; i <= high; i++) {

        if (src.at(i) < src.at(high)) {
            bIndex++;
            int tmp = src.at(i);
            src.at(i) = src.at(bIndex);
            src.at(bIndex) = tmp;
        }
    }
    bIndex++;
    int tmp = src.at(bIndex);
    src.at(bIndex) = src.at(high);
    src.at(high) = tmp;
    return bIndex;
}

void quickSort(vector<int>& src, int low, int high) {

    if (low >= high) {
        return;
    }
    int size = (int)src.size() - 1;
    int pivot = partition(src, low, high);
    quickSort(src, low, pivot - 1);
    quickSort(src, pivot + 1, high);
}

/*
方块 梅花 红桃 黑桃
*/
typedef enum {
   Diamond = 1,
   Club,
   Heart,
   Spade,
   Any
} CardType;

typedef enum {
    c0,
    c1,
    c2,
    c3,
    c4,
    c5,
    c6,
    c7,
    c8,
    c9,
    c10,
    c11,
    c12,
    c13,
    cAce, // 14,
    c15
} CardNumber;

typedef enum {
    RoyalFlush = 1,
    StraighFlush,
    FourOfAKind,
    FullHouse,
    Flush,
    Straigh,
    ThreeOfAKind,
    TwoPairs,
    OnePair,
    None
} ComboType;

struct Card {
    CardType type;
    CardNumber number;
};

int f(const vector<int> input) {
    
    vector<int> src = input;
    quickSort(src, 0, static_cast<int>(src.size()) - 1);

    int noList[15] = {0};
    int typeList[4] = {};

    // replacement count
    // 這裡假設一定會有一張大王一張小王 不然舊觀玲
    int count = 0;
    for (int i = 0; i < static_cast<int>(src.size()); i++) {
        if (src.at(i) == replace01 || src.at(i) == replace02) {
            count++;
        }
    }

    // 盡可能將type填值 例如大王小王鈞為黑桃 所以type +2
    for (int i = 0; i < 4 && count > 0; i++) {
        typeList[i] += count;
    }

    // int -> hex string -> struct Card
    vector<Card> cards;
    for (int i = 0; i < static_cast<int>(src.size()); i++) {
        if (src.at(i) == replace01 || src.at(i) == replace02) {
            Card card;
            card.number = (CardNumber)src.at(i);
            card.type = Any;
            cards.push_back(card);
        }
        else {
            stringstream sstream;
            sstream << std::hex << src.at(i);
            string result = sstream.str();
            string type = result.substr(0, 1);
            string num = result.substr(2, 1);
            Card card;
            card.type = (CardType)stoi(type);

            unsigned int inum;
            std::stringstream ss;
            ss << std::hex << num;
            ss >> inum;
            card.number = (CardNumber)(inum);
            cards.push_back(card);
        }
    }

    // SameType
    bool sameType = false;
    for (int i = 0; i < cards.size(); i++) {
        if (cards.at(i).number != replace01 && cards.at(i).number != replace02) {
            noList[cards.at(i).number] += 1;
            typeList[cards.at(i).type - 1] += 1;
            cout << typeList[cards.at(i).type - 1] << endl;
            cout << cards.size() << endl;
            if (typeList[cards.at(i).type - 1] >= cards.size()) {
                sameType = true;
            }
        }
    }

    bool royal = sameType;
    int rcount = count;
    for (int i = 14; i >= 10 && sameType; i--) {
        if (noList[i] != 1) {
            if (rcount > 0) {
                noList[i] = 1;
                rcount--;
            }
            else {
                royal = false;
            }
        }
    }
    if (royal) {
        return RoyalFlush;
    }
    int res = 1;
    int continueRes = 0;
    bool bContinue = false;
    for (int i = 1; i < 14; i++) {
        if (noList[i] != 0) {
            res *= noList[i];
            if (noList[i] == 4) {
                res = 12;
            }
        }
        if (noList[i] == 1) {
            continueRes++;
            if (continueRes == cards.size()) {
                bContinue = true;
            }
        }
        else {
            if (continueRes == cards.size()) {
                bContinue = true;
            }
            continueRes = 0;
        }
    }

    int ccount = count;

    if (bContinue && sameType) {
        return StraighFlush;
    }

    if (sameType && !bContinue) {
        return Flush;
    }

    if (!sameType && bContinue) {
        return Straigh;
    }

    //X X O C A --> X X C C A
    //X X O O A --> X X X O O
    //X X O O O --> X 0 0 0 0
    //X X O O O --> X 0 0 0 0
    if (count > 0) {
        res *= count;
    }

    switch (res) {
        case 12:
            return FourOfAKind;
        case 6:
            return FullHouse;
        case 3:
            return ThreeOfAKind;
        case 4:
            return TwoPairs;
        case 2:
            return OnePair;
    }
    
    return None;
}

TEST(CheckCards, RoyalFlush) {

    vector<int> values;
    values.push_back(266);
    values.push_back(267);
    values.push_back(268);
    values.push_back(269);
    values.push_back(270);
    EXPECT_EQ(f(values), RoyalFlush);
}

TEST(CheckCards, StraighFlush) {

    vector<int> values;
    values.push_back(266);
    values.push_back(267);
    values.push_back(268);
    values.push_back(269);
    values.push_back(265);
    EXPECT_EQ(f(values), StraighFlush);
}

TEST(CheckCards, FourOfAKind) {

    vector<int> values;
    values.push_back(258);
    values.push_back(514);
    values.push_back(770);
    values.push_back(1026);
    values.push_back(270);
    EXPECT_EQ(f(values), FourOfAKind);
}

TEST(CheckCards, FullHouse) {

    vector<int> values;
    values.push_back(265);
    values.push_back(521);
    values.push_back(777);
    values.push_back(780);
    values.push_back(1036);
    EXPECT_EQ(f(values), FullHouse);
}

TEST(CheckCards, Flush) {

    vector<int> values;
    values.push_back(514);
    values.push_back(521);
    values.push_back(526);
    values.push_back(522);
    values.push_back(518);
    EXPECT_EQ(f(values), Flush);
}

TEST(CheckCards, Straigh) {

    vector<int> values;
    values.push_back(258);
    values.push_back(515);
    values.push_back(772);
    values.push_back(1029);
    values.push_back(1030);
    EXPECT_EQ(f(values), Straigh);
}

TEST(CheckCards, ThreeOfAKind) {

    vector<int> values;
    values.push_back(260);
    values.push_back(516);
    values.push_back(772);
    values.push_back(1029);
    values.push_back(1030);
    EXPECT_EQ(f(values), ThreeOfAKind);
}

TEST(CheckCards, TwoPairs) {

    vector<int> values;
    values.push_back(260);
    values.push_back(516);
    values.push_back(773);
    values.push_back(1029);
    values.push_back(1030);
    EXPECT_EQ(f(values), TwoPairs);
}

TEST(CheckCards, OnePair) {

    vector<int> values;
    values.push_back(270);
    values.push_back(526);
    values.push_back(518);
    values.push_back(520);
    values.push_back(1030);
    EXPECT_EQ(f(values), OnePair);
}

TEST(CheckCardsW2Replacement, OnePair) {

    vector<int> values;
    values.push_back(269);
    values.push_back(268);
    values.push_back(774);
    values.push_back(replace01);
    values.push_back(replace02);
    EXPECT_EQ(f(values), OnePair);
}

TEST(CheckCardsW2Replacement, TwoPairs) {

    vector<int> values;
    values.push_back(258);
    values.push_back(514);
    values.push_back(773);
    values.push_back(replace01);
    values.push_back(replace02);
    EXPECT_EQ(f(values), TwoPairs);
}

TEST(CheckCardsW2Replacement, StraighFlush) {

    vector<int> values;
    values.push_back(269);
    values.push_back(268);
    values.push_back(265);
    values.push_back(267);
    values.push_back(266);
    EXPECT_EQ(f(values), StraighFlush);
}

TEST(CheckCardsW2Replacement, FullHouse) {

    vector<int> values;
    values.push_back(260);
    values.push_back(516);
    values.push_back(772);
    values.push_back(replace01);
    values.push_back(replace02);
    EXPECT_EQ(f(values), FullHouse);
}

TEST(CheckCardsW2Replacement, RoyalFlush) {

    vector<int> values;
    values.push_back(525);
    values.push_back(523);
    values.push_back(526);
    values.push_back(replace01);
    values.push_back(replace02);
    EXPECT_EQ(f(values), RoyalFlush);
}

int main(int argc, char* argv[]) {

    /*
      方块  2,    3,    4,    5,    6,    7,    8,    9,   10,    J,    Q,    K,    A
        0x102,0x103,0x104,0x105,0x106,0x107,0x108,0x109,0x10a,0x10b,0x10c,0x10d,0x10e
          258   259   260   261   262   263   264   265   266   267   268   269   270
      梅花
        0x202,0x203,0x204,0x205,0x206,0x207,0x208,0x209,0x20a,0x20b,0x20c,0x20d,0x20e
          514   515   516   517   518   519   520   521   522   523   524   525   526
      红桃
        0x302,0x303,0x304,0x305,0x306,0x307,0x308,0x309,0x30a,0x30b,0x30c,0x30d,0x30e
          770   771   772   773   774   775   776   777   778   779   780   781
      黑桃
        0x402,0x403,0x404,0x405,0x406,0x407,0x408,0x409,0x40a,0x40b,0x40c,0x40d,0x40e
         1026  1027  1028  1029  1030  1031  1032  1033  1034  1035  1036  1037  1038
      
      0x50f代表小王 - 1295
      0x610代表大王 - 1552
    //--------------------------------------------------------------------------------------------------
        2、同花顺：StraighFlush 如果花色一样，数字是连续的，皇家同花顺除外，例如[0x109,0x10a,0x10b,0x10c,0x10d],[0x10e,0x102,0x103,0x104,0x105]
        3、金刚：  FourOfAKind (7) 其中4张牌数字一样
        4、葫芦：  FullHouse (6) 其中3张牌数字一样，另外2张牌数字一样
        5、同花：  Flush  花色一样，数字不连续
        6、顺子：  Straigh   数字是连续，花色不一样
        7、三条：  ThreeOfAKind (3) 其中3张牌数字一样，另外2张牌数字不一样
        8、两对：  TwoPairs (4)其中2张牌数字一样，另外其中2张牌数字一样，最后一张数字不一样
        9、一对：  OnePair (2) 其中2张牌数字一样，另外数字不一样
    */

    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}